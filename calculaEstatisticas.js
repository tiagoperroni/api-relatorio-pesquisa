        let pesquisaA1 = 0;
        let pesquisaA2 = 0;
        let pesquisaA3 = 0;

        let pesquisaB1 = 0;
        let pesquisaB2 = 0;
        let pesquisaB3 = 0;
        let pesquisaB4 = 0;
        let pesquisaB5 = 0;

        let pesquisaC1 = 0;
        let pesquisaC2 = 0;
        let pesquisaC3 = 0;
        let pesquisaC4 = 0;
        let pesquisaC5 = 0;

        let pesquisaD1 = 0;
        let pesquisaD2 = 0;
        let pesquisaD3 = 0;
        let pesquisaD4 = 0;
        let pesquisaD5 = 0;
        let pesquisaD6 = 0;
        let pesquisaD7 = 0;
        let pesquisaD8 = 0;
        let pesquisaD9 = 0;
        let pesquisaD10 = 0;

        let pesquisaE1 = 0;
        let pesquisaE2 = 0;
        let pesquisaE3 = 0;

        let pesquisaF1 = 0;
        let pesquisaF2 = 0;
        let pesquisaF3 = 0;
        let pesquisaF4 = 0;
        let pesquisaF5 = 0;

        let novoRelatorio;

const estatisticas = (dados) => {    

    let dado = {};

        for (let i = 0; i < dados.length; i++) {         
        
        let atendida = dado['atendida'] = dados[i].atendida;
        let notaAtendimento = dado['notaAtendimento'] = dados[i].notaAtendimento;
        let atendente =  dado['atendente'] = dados[i].atendente;
        let notaProduto = dado['notaProduto'] = dados[i].notaProduto;
        let genero = dado['genero'] = dados[i].genero;
        let idade = dado['idade'] = dados[i].idade;
        let timestamp = dado['timestamp'] = dados[i].timestamp;

        const novaIdade = JSON.parse(idade)

        if(atendida === "Não foi atendida"){
            pesquisaA1++;            
        } else if (atendida === "Parcialmente atendida") {
            pesquisaA2++;
        }
        else if(atendida === "Totalmente atendida") {
            pesquisaA3++;
        }

        if(notaAtendimento === "Péssimo"){
            pesquisaB1++;            
        } else if (notaAtendimento === "Ruim") {
            pesquisaB2++;
        }
        else if(notaAtendimento === "Aceitável") {
            pesquisaB3++;
        }
        else if(notaAtendimento === "Bom") {
            pesquisaB4++;
        } 
        else if(notaAtendimento === "Excelente") {
             pesquisaB5++;
        } 

        if(atendente === "Indelicado"){
            pesquisaC1++;            
        } else if (atendente === "Mau humorado") {
            pesquisaC2++;
        }
        else if(atendente === "Neutro") {
            pesquisaC3++;
        }
        else if(atendente === "Educado") {
            pesquisaC4++;
        } 
        else if(atendente === "Muito Atencioso") {
            pesquisaC5++;
        }

        if(notaProduto === "1"){
            pesquisaD1++;            
        } 
        else if (notaProduto === "2") {
            pesquisaD2++;
        }
        else if(notaProduto === "3") {
            pesquisaD3++;
        }
        else if(notaProduto === "4") {
            pesquisaD4++;
        } 
        else if(notaProduto === "5") {
            pesquisaD5++;
        }
        else if (notaProduto === "6") {
            pesquisaD6++;
        }
        else if(notaProduto === "7") {
            pesquisaD7++;
        }
        else if(notaProduto === "8") {
            pesquisaD8++;
        } 
        else if(notaProduto === "9") {
            pesquisaD9++;
        }
        else if(notaProduto === "10") {
            pesquisaD10++;
        }

        if(genero === "Masculino"){
            pesquisaE1++;            
        } 
        else if (genero === "Feminino") {
            pesquisaE2++;
        }
        else if(genero === "LGBT") {
            pesquisaE3++;
        }

        if(novaIdade < 15){
            pesquisaF1++;            
        } 
        else if (novaIdade > 15 && novaIdade < 21 ) {
            pesquisaF2++;
        }
        else if(novaIdade > 22 && novaIdade < 35 ) {
            pesquisaF3++;
        }
        else if(novaIdade > 36 && novaIdade < 50 ) {
            pesquisaF4++;
        } 
        else if(novaIdade > 50 ) {
            pesquisaF5++;
        }         
                 
        novoRelatorio = {
            
            a: {

                naofoiatendida: pesquisaA1,
                parcialmenteatendida: pesquisaA2,
                totalmenteatendida: pesquisaA3,
                naofoiatendida: pesquisaA1,
                parcialmenteatendida: pesquisaA2,
                totalmenteatendida: pesquisaA3,
                
            },
               b: {
                
                pessimo: pesquisaB1,
                ruim: pesquisaB2,
                aceitavel: pesquisaB3,
                bom: pesquisaB4,
                excelente: pesquisaB5,
            
            },
               c: {
                
                indelicado: pesquisaC1,
                mauhumorado: pesquisaC2,
                neutro: pesquisaC3 ,
                educado: pesquisaC4,
                muitoatencioso: pesquisaC5,
                
            },
               d: {
                
                nota1: pesquisaD1,
                nota2: pesquisaD2,
                nota3: pesquisaD3,
                nota4: pesquisaD4,
                nota5: pesquisaD5,
                nota6: pesquisaD6,
                nota7: pesquisaD7,
                nota8: pesquisaD8,
                nota9: pesquisaD9,
                nota10: pesquisaD10,
            },
            
            e: {

                masculino: pesquisaE1,
                feminino: pesquisaE2, 
                lgbt: pesquisaE3,
            },

            f: {

                menos15: pesquisaF1,
                entre15e21: pesquisaF2, 
                entre22e35: pesquisaF3,
                entree36e50: pesquisaF4,
                maiorque50: pesquisaF5,
            }            
        }         
    }   
                pesquisaA1 = 0;
                pesquisaA2 = 0;
                pesquisaA3 = 0;

                pesquisaB1 = 0;
                pesquisaB2 = 0;
                pesquisaB3 = 0;
                pesquisaB4 = 0;
                pesquisaB5 = 0;

                pesquisaC1 = 0;
                pesquisaC2 = 0;
                pesquisaC3 = 0;
                pesquisaC4 = 0;
                pesquisaC5 = 0;

                pesquisaD1 = 0;
                pesquisaD2 = 0;
                pesquisaD3 = 0;
                pesquisaD4 = 0;
                pesquisaD5 = 0;
                pesquisaD6 = 0;
                pesquisaD7 = 0;
                pesquisaD8 = 0;
                pesquisaD9 = 0;
                pesquisaD10 = 0;

                pesquisaE1 = 0;
                pesquisaE2 = 0;
                pesquisaE3 = 0;

                pesquisaF1 = 0;
                pesquisaF2 = 0;
                pesquisaF3 = 0;
                pesquisaF4 = 0;
                pesquisaF5 = 0;

                return novoRelatorio;
        
}

module.exports = estatisticas;