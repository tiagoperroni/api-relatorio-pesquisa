const dotenv = require('dotenv');
dotenv.config();

const express = require('express');
const app = express();
const morgan = require('morgan');
const cors = require('cors');

import './src/database';
import userRoutes from './src/routes/user.routes';
import tokenRoutes from './src/routes/token.routes';

const PORT = process.env.SERVER_PORT || 3002;

app.use(morgan('dev'));
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false} ));


app.get('/', (req, res) => {
    try{
        res.status(200).sendFile(`${__dirname}/view/index.html`);
    }catch(error) {
        res.status(500).json({ message: error })
    }
});


app.use('/', cors(), require('./src/routes/estatisticas.routes'));
app.use('/users/', userRoutes);
app.use('/tokens/', tokenRoutes);

app.listen(PORT, () => {
    console.log(`Server started on por ${PORT}`);
});
