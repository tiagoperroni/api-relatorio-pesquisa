const axios = require('axios');

const request = (url, method, data) => {
    return axios({ url, method, data});

}

test.only('deverá obter a pesquisa', async () => {
    const response = await request('http://localhost:3001/pesquisa', 'get', {
        QueryParams: {
            Authorization: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NDEsImVtYWlsIjoibWFyaWFlZHVhcmRhQGdtYWlsLmNvbSIsImlhdCI6MTYzMTI4MzM1MCwiZXhwIjoxNjMxODg4MTUwfQ.O1jJLhdvtc4q71UlqVI2UE0vRhXQ37FjVOZHRAMsTZs'
        }
    });
    console.log(response);
    //expect(response.status).toBe(401);
});

test('deverá obter as estatisticas', async () => {
    const response = await request('http://localhost:3001/estatisticas', 'get');
    expect(response.status).toBe(200);
});

test('deverá obter o relatorio', async () => {
    const response = await request('http://localhost:3001/relatorio', 'get');
    expect(response.status).toBe(200);
});

test('deverá criar gerar um novo dado', async () => {
    const data = {    
        id: null,
        atendida: 'Parcialmente atendida', 
        notaAtendimento: 'Aceitável', 
        atendente: 'Educado', 
        notaProduto: '4', 
        genero: 'Masculino', 
        idade: '36',
        timestamp: Date.now()};        
    //const enviar = estatisticasController.postPesquisa(JSON.stringify(data));
    //console.log(enviar);
    const response = await request('http://localhost:3001/pesquisa', 'post', data);
    const estatistica = response.data;    
    expect(estatistica.status).toBe(200);      
    expect(estatistica.mensagem).toBe('Pesquisa Registrada Com sucesso');
    // console.log(estatistica.obj.id);
    await request(`http://localhost:3001/pesquisa/${estatistica.obj.id}`, 'delete');
   
});

test('deverá deletar um novo dado', async () => {
    const data = {    
        id: null,
        atendida: 'Parcialmente atendida', 
        notaAtendimento: 'Aceitável', 
        atendente: 'Educado', 
        notaProduto: '4', 
        genero: 'Masculino', 
        idade: '36',
        timestamp: Date.now()};      
    const response = await request('http://localhost:3001/pesquisa', 'post', data);    
    const deleteResponse = await request(`http://localhost:3001/pesquisa/${response.data.obj.id}`, 'delete');
    expect(deleteResponse.data.status).toBe(204);      
    expect(deleteResponse.data.mensagem).toBe('Registro deletado com sucesso');   
   
});

