const estatisticas = require("../../calculaEstatisticas");
const axios = require('axios');

const request = (url, method, data) => {
    return axios(({ url, method, data }));
}

exports.getPesquisa = async (req, res) => {
    try{
       const response =  await request('http://localhost:3000/pesquisa', 'get');
        res.status(200).send(response.data) ;
    }catch(error) {
        res.status(500).json({ message: error })
    }
};

exports.getEstatisticas = async (req, res) => {
    try{
       const response =  await request('http://localhost:3000/pesquisa', 'get');

       if (!response) {
           return res.status(400).send('Houve um erro ao tentar trazer os dados')
       }

        const dados = await estatisticas(response.data);
        console.log(dados);       
        res.status(200).send(dados);

    }catch(error) {
        console.log(error);
        res.status(500).json({ message: error })
    }
};

exports.getRelatorio = async (req, res) => {
    try{
       const response =  await request('http://localhost:3000/pesquisa', 'get');

       if (!response) {
           return res.status(400).send('Houve um erro ao tentar trazer os dados')
       }

       const dados = await estatisticas(response.data);


       console.log(dados);

        res.status(200).send(
            `
            <html>
                <head>
                    <meta charset="utf-8">
                </head>
                <body>
                    <p>Sua solicitação foi atendida?</>
                    <p>${JSON.stringify(dados.a)}</p>
                    <p>Qual nota você daria para o atendimento?</p>
                    <p>${JSON.stringify(dados.b)}</p>
                    <p>Como você classificaria o comportamento do atendente?</>
                    <p>${JSON.stringify(dados.c)}</p>
                    <p>Qual nota você daria para o atendimento?</p>
                    <p>${JSON.stringify(dados.d)}</p>
                    <p>Informa seu Gênero</>
                    <p>${JSON.stringify(dados.e)}</p>
                    <p>Idade:</>
                    <p>${JSON.stringify(dados.f)}</p>
                </body>
            </html>
        `
        );

    }catch(error) {
        res.status(500).json({ message: error })
    }
};

exports.postPesquisa = async (req, res) => {
<<<<<<< HEAD
         const response = req.body;
         
    try{
        // const { id, atendida, notaAtendimento, atendente, notaProduto, genero, idade, timestamp } = response;     

        const obj = {
            id: response.id,
            atendida: response.atendida,
            notaAtendimento: response.notaAtendimento,
            atendente: response.atendente,
            notaProduto: response.notaProduto,
            genero: response.genero,
            idade: response.idade,
            timestamp: response.timestamp
        } 
            console.log(obj);
    
              const newObj = await request('http://localhost:3000/pesquisa', 'post', obj);        
              res.status(200).json({status: 200, mensagem:'Pesquisa Registrada Com sucesso', obj: newObj.data});

=======

    try{

      const data = {
        id,
        atendida,
        notaAtendimento,
        atendente,
        notaProduto,
        genero,
        idade,
        timestamp,
    } = req.body;

        const newObj = await request('http://localhost:3000/pesquisa', 'post', data);
        res.status(200).json({status: 200, messagem:'Pesquisa Registrada Com sucesso', id: newObj.data.id});
>>>>>>> 2eda10b242046c1b9f7b73eab946aabad03b84b7
    }catch (err) {
               console.log(err);
        res.status(500).json({ err })
    }
};

exports.deleteData = async (req, res) => {

    try{
        const data = req.params.id;
    
    try {

        await request(`http://localhost:3000/pesquisa/${data}`, 'delete');
        return res.status(200).json({status: 204, mensagem:'Registro deletado com sucesso', id: data});

    } catch (error) {
        if (error.message === "Request failed with status code 404") {
            return res.status(404).json({ msg: `Não foi encontrado com id: ${data}` })
        }
        return res.status(500).json({ error })
    }

    }catch (err) {
        console.log(err);
        res.status(500).json({ message: err });
    }
}
