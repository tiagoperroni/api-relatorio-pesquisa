const router = require('express').Router();
import tokenController from '../controllers/TokenController';


router.post('/', tokenController.post);

export default router;
