const express = require('express').Router();
const estatisticasController = require('../controllers/estatisticas.controller');

import loginRequired from '../middleware/loginRequired';

express.get('/pesquisa', loginRequired, estatisticasController.getPesquisa);
express.get('/estatisticas', estatisticasController.getEstatisticas);
express.get('/relatorio', estatisticasController.getRelatorio);
express.post('/pesquisa', estatisticasController.postPesquisa);
express.delete('/pesquisa/:id', estatisticasController.deleteData);

module.exports = express;
