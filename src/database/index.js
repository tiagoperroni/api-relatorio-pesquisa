import { Sequelize } from "sequelize";
import databaseConfig from '../config/database';
import User from "../models/User";


const models = User;

const connection = new Sequelize(databaseConfig);
try {
    models.init(connection);
    console.log('Connect database success');
} catch (error) {
    console.log(error);
}

